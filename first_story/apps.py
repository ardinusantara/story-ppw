from django.apps import AppConfig


class FirstStoryConfig(AppConfig):
    name = 'first_story'
