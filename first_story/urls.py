from django.urls import path, include
from . import views

app_name = "first_story"
urlpatterns = [
    path('', views.story1, name="story1"),
]
