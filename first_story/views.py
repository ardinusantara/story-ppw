from django.shortcuts import render
from django.http import HttpResponse

def story1(request):
    return render(request, 'first_story/story1.html')
