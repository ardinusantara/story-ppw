from django.db import models
from django.urls import reverse

class Matkul(models.Model):
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.IntegerField()
    semester = models.CharField(max_length=100)
    ruangan = models.CharField(max_length=100)
    deskripsi = models.TextField()

    def get_absolute_url(self):
        return reverse('home')