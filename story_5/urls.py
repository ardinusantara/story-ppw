from django.urls import path
from .views import (
    MatkulListView,
    MatkulDetailView,
    MatkulCreateView,
    MatkulUpdateView,
    MatkulDeleteView
)
from . import views

urlpatterns = [
    path('', MatkulListView.as_view(), name='home'),
    path('matkul/<int:pk>/', MatkulDetailView.as_view(), name='matkul-detail'),
    path('matkul/create/', MatkulCreateView.as_view(), name='matkul-create'),
    path('matkul/<int:pk>/update/', MatkulUpdateView.as_view(), name='matkul-update'),
    path('matkul/<int:pk>/delete/', MatkulDeleteView.as_view(), name='matkul-delete'),
]
