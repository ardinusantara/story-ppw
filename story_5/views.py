from django.shortcuts import render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Matkul

class MatkulListView(ListView):
    model = Matkul
    template_name = 'story_5/home.html'
    context_object_name = 'matkuls'

class MatkulDetailView(DetailView):
    model = Matkul

class MatkulCreateView(CreateView):
    model = Matkul
    fields = ['nama', 'dosen', 'sks', 'semester', 'ruangan', 'deskripsi']

class MatkulUpdateView(UpdateView):
    model = Matkul
    fields = ['nama', 'dosen', 'sks', 'semester', 'ruangan', 'deskripsi']

class MatkulDeleteView(DeleteView):
    model = Matkul
    success_url = '/story_5'