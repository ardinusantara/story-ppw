# Generated by Django 3.0.8 on 2020-10-16 14:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story_5', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='matkul',
            old_name='matkul',
            new_name='nama',
        ),
    ]
